%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% DiNi(2022) %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%% Function that Manage ErrorsCalc and Messages %%%%%%%%%%%%
%%%%%%%%%%%%%%%%%% M.Spitoni - Created on 09/02/2023 %%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%input: fig as app.UIFigure, Data as app.UITable.Data
%output: Data as app.UITable.Data containing errors calcultated with custum
%function f_errcalc

function Data = f_ReadDiniError(fig,Data,row,col,myH,myD,readdini)
    
    errorDiNi  =  ["E202","E320","E321","E323","E327"]; 
    errorTITLE = ["Error E202","Error E320","Error E321","Error E323","Error E327"];
    
    if readdini == errorDiNi(1) %Error
        msg = "Compensator out of range - Correct the leveling of instrument" + ...
            ", otherwise contact the service";
        title = errorTITLE(1);
        uialert(fig, msg, title,Icon="warning")
        Data{row,col} = myH; 
        Data{row,(col+6)} = myD; 
    elseif readdini == errorDiNi(2)
        msg = "Runtime error - Repeat the measurement";
        title = errorTITLE(2);
        uialert(fig, msg, title,Icon="warning")
        Data{row,col} = myH; 
        Data{row,(col+6)} = myD; 
    elseif readdini == errorDiNi(3)
        msg = "Change of brightness too great - Repeat the measurement";
        title = errorTITLE(3);
        uialert(fig, msg, title,Icon="warning")
        Data{row,col} = myH; 
        Data{row,(col+6)} = myD; 
    elseif readdini == errorDiNi(4)
        msg = "Staff cannot be read - Check the conditions of measurement " + ...
            "process - Repeat measurement";
        title = errorTITLE(4);
        uialert(fig, msg, title,Icon="warning")
        Data{row,col} = myH; 
        Data{row,(col+6)} = myD; 
     elseif readdini == errorDiNi(5)
        msg = "Staff section too small - Staff section is not symmetrical for measurement";
        title = errorTITLE(5);
        uialert(fig, msg, title,Icon="warning")
        Data{row,col} = myH; 
        Data{row,(col+6)} = myD; 
    else 
        Data{row,col} = myH*100000; 
        Data{row,(col+6)} = round(myD,3); 
        mytable = Data;
        [TE12,TE13,TE23] = f_errcalc(mytable);
        Data.E_12 = TE12(:,1); 
        Data.E_13 = TE13(:,1);
        Data.E_23 = TE23(:,1);
        
    end

                       
end
